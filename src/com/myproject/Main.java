package src.com.myproject;
import java.util.ArrayList;
import java.util.List;

/**
 * La classe Main contient la méthode principale (main) qui sert de point d'entrée du programme.
 */
public class Main {

    public static void main(String[] args) {

        // Création d'une voiture
        Voiture v1 = new Voiture("BMW", "1", "Rouge");

        // Création d'une liste de vêtements
        List<Vetement> lstVetement = new ArrayList<Vetement>();
        Vetement unVetement;

        // Ajout de vêtements à la liste
        lstVetement.add(unVetement = new Vetement("T-shirt", "Blanc", Tailles.M));
        lstVetement.add(unVetement = new Vetement("Pantalon", "Bleu", Tailles.L));
        lstVetement.add(unVetement = new Vetement("Chaussures", "Rouge", Tailles.XXXL));

        // Création de deux personnages
        Personnage p1 = new Personnage("Pierre", "M", 150, 10, lstVetement);
        Personnage p2 = new Personnage("Jacques", "M", 184, 1, lstVetement);

        // Affichage des caractéristiques du personnage p1
        System.out.println(p1.getTaille() + "cm");
        System.out.println(p1.getTailleTotale() + "cm");

        // Attribution du propriétaire à la voiture v1
        v1.setProprietaire(p1);

        System.out.println();
        System.out.println(v1.toString() + "\n");
        System.out.println(p1.toString() + "\n");

        // Actions sur la voiture v1
        v1.arreter();
        v1.demarrer();
        v1.demarrer();
        v1.arreter();

        System.out.println();
        v1.detruireMoteur();
        System.out.println();

        v1.arreter();
        v1.demarrer();
        v1.demarrer();
        v1.arreter();

        System.out.println();
        System.out.println(v1.toString());

        // Changement de propriétaire de la voiture v1
        v1.setProprietaire(p2);
        System.out.println(v1.toString());
        System.out.println();



        /////////////////////////////////
        //          Garagiste          //
        /////////////////////////////////
        Garagiste garagiste = new Garagiste();
        System.out.println();
        garagiste.listerCompetences();
        System.out.println();
        garagiste.effectuerLeTravail(3);
        System.out.println();
        garagiste.entrerGarage(v1);
        System.out.println();

        System.out.println("Point de vie du véhicule " + v1.getMarque() + " " + v1.getModele() + " : " + v1.getpointsDeVie());
        System.out.println("Point de vie du moteur du véhicule " + v1.getMarque() + " " + v1.getModele() + " : " + v1.getMoteur());
        System.out.println();

        garagiste.effectuerLeTravail(1);

        System.out.println();

        garagiste.effectuerLeTravail(2);

        System.out.println();
        System.out.println("Point de vie du véhicule " + v1.getMarque() + " " + v1.getModele() + " : " + v1.getpointsDeVie());
        System.out.println("Point de vie du moteur du véhicule " + v1.getMarque() + " " + v1.getModele() + " : " + v1.getMoteur());

        System.out.println();
        System.out.println("Couleur du véhicule " + v1.getMarque() + " " + v1.getModele() + " : " + v1.getCouleur());
        System.out.println();
        garagiste.effectuerLeTravail(3);
        System.out.println();
        System.out.println("Couleur du véhicule " + v1.getMarque() + " " + v1.getModele() + " : " + v1.getCouleur());



        //////////////////////////////////
        //          Coiffeur           //
        /////////////////////////////////
        Coiffeur coiffeur = new Coiffeur();
        System.out.println();
        coiffeur.listerCompetences();
        System.out.println();
        coiffeur.effectuerLeTravail(3);
        System.out.println();
        coiffeur.entrerCoiffeur(p1);
        System.out.println();

        System.out.println("Taille de cheveux de la personne " + p1.getPseudo() + " : " + p1.getLongeurCheveux() + "cm");


        System.out.println();

        coiffeur.effectuerLeTravail(1);

        System.out.println();
        System.out.println("Taille de cheveux de la personne " + p1.getPseudo() + " : " + p1.getLongeurCheveux() + "cm");

        System.out.println();
        coiffeur.effectuerLeTravail(2);

        System.out.println();
        coiffeur.sortieCoiffeur();
    }
}