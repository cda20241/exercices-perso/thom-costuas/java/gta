package src.com.myproject;
/**
 * La classe Vetement représente un vêtement avec son type, sa couleur et sa taille.
 */
public class Vetement {

    private String Type;      // Type du vêtement
    private String Couleur;   // Couleur du vêtement
    private Tailles Taille;   // Taille du vêtement

    /**
     * Constructeur de la classe Vetement.
     *
     * @param type   Le type du vêtement.
     * @param couleur La couleur du vêtement.
     * @param taille  La taille du vêtement.
     */
    public Vetement(String type, String couleur, Tailles taille) {
        Type = type;
        Couleur = couleur;
        Taille = taille;
    }

    /**
     * Getter pour récupérer le type du vêtement.
     *
     * @return Le type du vêtement.
     */
    public String getType() {
        return Type;
    }

    /**
     * Setter pour définir le type du vêtement.
     *
     * @param type Le nouveau type du vêtement.
     */
    public void setType(String type) {
        Type = type;
    }

    /**
     * Getter pour récupérer la couleur du vêtement.
     *
     * @return La couleur du vêtement.
     */
    public String getCouleur() {
        return Couleur;
    }

    /**
     * Setter pour définir la couleur du vêtement.
     *
     * @param couleur La nouvelle couleur du vêtement.
     */
    public void setCouleur(String couleur) {
        Couleur = couleur;
    }

    /**
     * Getter pour récupérer la taille du vêtement.
     *
     * @return La taille du vêtement.
     */
    public Tailles getTaille() {
        return Taille;
    }

    /**
     * Setter pour définir la taille du vêtement.
     *
     * @param taille La nouvelle taille du vêtement.
     */
    public void setTaille(Tailles taille) {
        Taille = taille;
    }

    /**
     * Méthode toString pour obtenir une représentation textuelle du vêtement.
     *
     * @return Une chaîne de caractères représentant le vêtement.
     */
    @Override
    public String toString() {
        return "Vetement{" +
                "Type='" + Type + '\'' +
                ", Couleur='" + Couleur + '\'' +
                ", Taille=" + Taille +
                '}';
    }
}
