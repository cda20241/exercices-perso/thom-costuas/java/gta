package src.com.myproject;
import java.util.List;

/**
 * Interface représentant un métier dans le jeu.
 */
public interface Metier {

    /**
     * Liste les compétences associées à ce métier.
     *
     * @return Une liste de chaînes représentant les compétences du métier.
     */
    List<String> listerCompetences();

    /**
     * Effectue le travail associé à une compétence spécifiée.
     *
     * @param idCompetence L'identifiant de la compétence à réaliser.
     */
    void effectuerLeTravail(int idCompetence);

    /**
     * Facture le client pour le travail effectué et affiche une facture.
     */
    void facturerClient();
}
