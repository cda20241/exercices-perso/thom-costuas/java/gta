package src.com.myproject;
/**
 * L'énumération Tailles représente différentes tailles de vêtements.
 */
public enum Tailles {
    S,   // Petite
    M,   // Moyenne
    L,   // Grande
    XL,  // Très Grande
    XXL, // Extra Très Grande
    XXXL // Triple Extra Très Grande
}

