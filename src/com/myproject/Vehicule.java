package src.com.myproject;
/**
 * La classe abstraite Vehicule représente un véhicule générique avec des caractéristiques telles que la marque,
 * le modèle, la couleur, les points de vie, le moteur, l'état de marche et le propriétaire.
 */
public abstract class Vehicule {
    private String marque;
    private String modele;
    private String couleur;
    private int pointsDeVie;
    private float moteur;
    private Boolean enMarche;
    private Personnage proprietaire;

    /**
     * Constructeur de la classe Vehicule.
     *
     * @param marque  La marque du véhicule.
     * @param modele  Le modèle du véhicule.
     * @param couleur La couleur du véhicule.
     */
    public Vehicule(String marque, String modele, String couleur) {
        this.marque = marque;
        this.modele = modele;
        this.couleur = couleur;
        this.pointsDeVie = 100;
        this.moteur = 100;
        this.enMarche = false;
        this.proprietaire = null;
    }

    // Méthodes getters et setters

    /**
     * @return La marque du véhicule.
     */
    public String getMarque() {
        return marque;
    }

    /**
     * Définit la marque du véhicule.
     *
     * @param marque La nouvelle marque du véhicule.
     */
    public void setMarque(String marque) {
        this.marque = marque;
    }

    /**
     * @return Le modèle du véhicule.
     */
    public String getModele() {
        return modele;
    }

    /**
     * Définit le modèle du véhicule.
     *
     * @param modele Le nouveau modèle du véhicule.
     */
    public void setModele(String modele) {
        this.modele = modele;
    }

    /**
     * @return La couleur du véhicule.
     */
    public String getCouleur() {
        return couleur;
    }

    /**
     * Définit la couleur du véhicule.
     *
     * @param couleur La nouvelle couleur du véhicule.
     */
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    /**
     * @return Les points de vie du véhicule.
     */
    public int getpointsDeVie() {
        return pointsDeVie;
    }

    /**
     * Définit les points de vie du véhicule.
     *
     * @param pointsDeVie Les nouveaux points de vie du véhicule.
     */
    public void setpointsDeVie(int pointsDeVie) {
        this.pointsDeVie = pointsDeVie;
    }

    /**
     * @return Le niveau du moteur du véhicule.
     */
    public float getMoteur() {
        return moteur;
    }

    /**
     * Définit le niveau du moteur du véhicule.
     *
     * @param moteur Le nouveau niveau du moteur du véhicule.
     */
    public void setMoteur(float moteur) {
        this.moteur = moteur;
    }

    /**
     * @return Le propriétaire actuel du véhicule.
     */
    public Personnage getProprietaire() {
        return proprietaire;
    }

    /**
     * Affecte un nouveau propriétaire au véhicule.
     *
     * @param proprietaire Le nouveau propriétaire du véhicule.
     * @return true si le changement de propriétaire est réussi, false sinon.
     */
    public boolean setProprietaire(Personnage proprietaire) {
        if (!estHS() && this.proprietaire == null) {
            this.proprietaire = proprietaire;
            return true;
        } else {
            System.out.println(this.marque + this.modele + " : Impossible d'effectuer l'action ! : Changement de Propriétaire");
            return false;
        }
    }

    /**
     * Démarre le moteur du véhicule.
     *
     * @return true si le moteur démarre avec succès, false sinon.
     */
    public boolean demarrer() {
        if (this.enMarche == false && !estHS()) {
            this.enMarche = true;
            System.out.println(this.marque + this.modele + " : Moteur démarré");
            return true;
        } else {
            System.out.println(this.marque + this.modele + " : Impossible d'effectuer l'action ! : Démarrer");
            return false;
        }
    }

    /**
     * Arrête le moteur du véhicule.
     *
     * @return true si le moteur s'arrête avec succès, false sinon.
     */
    public boolean arreter() {
        if (this.enMarche == true && !estHS()) {
            this.enMarche = false;
            System.out.println(this.marque + this.modele + " : Moteur arrêté");
            return true;
        } else {
            System.out.println(this.marque + this.modele + " : Impossible d'effectuer l'action ! : Arrêter");
            return false;
        }
    }

    /**
     * Détruit complètement le véhicule.
     */
    public void detruireVehicule() {
        this.pointsDeVie = 0;
        this.setProprietaire(null);
        System.out.println("Véhicule détruit ! ");
    }

    /**
     * Détruit le moteur du véhicule.
     */
    public void detruireMoteur() {
        this.moteur = 0;
        System.out.println("Moteur détruit ! ");
    }

    /**
     * Vérifie si le véhicule est hors service.
     *
     * @return true si le véhicule est hors service, false sinon.
     */
    public boolean estHS() {
        return vehiculeHS() || moteurHS();
    }

    /**
     * Vérifie si le moteur du véhicule est hors service.
     *
     * @return true si le moteur est hors service, false sinon.
     */
    public boolean moteurHS() {
        return this.moteur == 0;
    }

    /**
     * Vérifie si le véhicule est hors service.
     *
     * @return true si le véhicule est hors service, false sinon.
     */
    public boolean vehiculeHS() {
        return this.pointsDeVie == 0;
    }

    /**
     * @return Une représentation textuelle du véhicule.
     */


    @Override
    public String toString() {
        return "Vehicule{" +
                "marque='" + marque + '\'' +
                ", modele='" + modele + '\'' +
                ", couleur='" + couleur + '\'' +
                ", pointsDeVie=" + pointsDeVie +
                ", moteur=" + moteur +
                ", enMarche=" + enMarche +
                ", proprietaire=" + proprietaire +
                '}';
    }
}
