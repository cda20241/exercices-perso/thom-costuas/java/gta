package src.com.myproject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Classe représentant un Coiffeur dans le jeu, implémentant l'interface Metier.
 */
public class Coiffeur implements Metier {

    private Map<Integer, String> competences;
    private Map<Integer, Integer> coupes;
    private Personnage client;

    /**
     * Constructeur de la classe Coiffeur.
     * Initialise les compétences du coiffeur, le client et les différentes coupes disponibles.
     */
    public Coiffeur() {
        this.competences = new HashMap<>();
        this.client = null;
        this.competences.put(1, "Faire une coupe");
        this.competences.put(2, "Poser des extensions");

        this.coupes = new HashMap<>();
        this.coupes.put(1, 0);
        this.coupes.put(2, 3);
        this.coupes.put(3, 9);
        this.coupes.put(4, 22);
        this.coupes.put(5, 40);
    }

    /**
     * Permet à un personnage d'entrer dans le salon de coiffure.
     *
     * @param unPersonnage Le personnage qui entre dans le salon de coiffure.
     * @return true si l'entrée est réussie, false si le salon de coiffure est occupé.
     */
    public boolean entrerCoiffeur(Personnage unPersonnage) {
        if (coiffeurEstVide()) {
            System.out.println("Entrée dans le salon de coiffure....");
            this.client = unPersonnage;
            return true;
        } else {
            System.out.println("Un client est déjà dans le salon de coiffure !");
            return false;
        }
    }

    /**
     * Permet à un personnage de sortir du salon de coiffure.
     *
     * @return true si la sortie est réussie, false si le salon de coiffure est vide.
     */
    public boolean sortieCoiffeur() {
        if (!coiffeurEstVide()) {
            System.out.println("Sortie du salon de coiffure....");
            this.client = null;
            return true;
        } else {
            System.out.println("Aucun client n'est dans le salon de coiffure !");
            return false;
        }
    }

    /**
     * Vérifie si le salon de coiffure est vide.
     *
     * @return true si le salon de coiffure est vide, false sinon.
     */
    public boolean coiffeurEstVide() {
        return this.client == null;
    }

    @Override
    public List<String> listerCompetences() {
        System.out.println("Liste des compétences :");
        System.out.println();
        competences.forEach((id, competence) -> System.out.println(id + ". " + competence));
        return List.copyOf(competences.values());
    }

    @Override
    public void effectuerLeTravail(int idCompetence) {
        if (!coiffeurEstVide()) {
            if (competences.containsKey(idCompetence)) {
                System.out.println("Compétence sélectionnée : " + competences.get(idCompetence));

                switch (idCompetence) {
                    case 1:
                        faireUneCoupe();
                        break;
                    case 2:
                        poserExtensions();
                        break;
                    default:
                        System.out.println("Compétence inconnue");
                }
            } else {
                System.out.println("Compétence inconnue");
            }
        } else {
            System.out.println("Il n'y a aucun client dans le salon de coiffure !");
        }
    }

    /**
     * Effectue l'opération de faire une coupe sur le client.
     */
    public void faireUneCoupe() {
        boolean choixValide = false;
        //On boucle tant que le choix de l'utilisateur n'est pas valide.
        while (!choixValide) {
            System.out.println("Choisissez une coupe parmi les options suivantes:");
            this.coupes.forEach((num, longueur) ->
                    System.out.println(num + ". " + getDetailCoupe(longueur)));
            System.out.println("Entrez 'exit' pour annuler l'opération.");

            Scanner scanner = new Scanner(System.in);
            String choixCoupe = scanner.nextLine();

            if (choixCoupe.equalsIgnoreCase("exit")) {
                System.out.println("Opération annulée !");
                return;
            }
            //Si le test passe c'est que le choix de l'utilisateur est valide
            if (validerChoixCoupe(choixCoupe)) {
                processChoixCoupe(Integer.parseInt(choixCoupe)); 
                choixValide = true;
            } else {
                System.out.println("Choix de coupe invalide ! Veuillez réessayer.");
            }
    
        }
    }


    public void processChoixCoupe(int choixCoupe) {
        int longueur = this.coupes.get(choixCoupe);
        System.out.println("Retouches de la coupe en cours...");
        //Méthodes try catch permettant d'utiliser la méthode thread.sleep
        try {
            int tempsCoupe = 3000;
            Thread.sleep(tempsCoupe);
            this.client.setLongeurCheveux(longueur);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Retouches terminées !");
    }



    /**
     * Valide le choix de coupe saisi par l'utilisateur.
     *
     * @param choix La chaîne représentant le choix de coupe.
     * @return true si le choix est valide, false sinon.
     */
    private boolean validerChoixCoupe(String choix) {
        //On vérifie que le choix de l'utilisateur est bien dans la plage de valeur attendues
        try {
            int choixInt = Integer.parseInt(choix);
            return choixInt >= 1 && choixInt <= 5;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Obtient le détail d'une coupe en fonction de sa longueur.
     *
     * @param longueur La longueur de la coupe.
     * @return Le détail de la coupe.
     */
    private String getDetailCoupe(int longueur) {
        return switch (longueur) {
            case 0 -> "Zidane (0cm)";
            case 3 -> "Militaire (3cm)";
            case 9 -> "Courts (9cm)";
            case 22 -> "Mi-longs (22cm)";
            case 40 -> "Longs (40cm)";
            default -> "Inconnu";
        };
    }

    /**
     * Effectue l'opération de poser des extensions sur le client.
     */
    public void poserExtensions() {
        boolean choixValide = false;
        //On boucle tant que le choix de l'utilisateur n'est pas valide.
        while (!choixValide) {
            System.out.println("Quelle longueur d'extensions souhaitez-vous ? (40cm max) :");
            System.out.println("Entrez 'exit' pour annuler l'opération.");

            Scanner scanner = new Scanner(System.in);
            String choixExtension = scanner.nextLine();

            if (choixExtension.equalsIgnoreCase("exit")) {
                System.out.println("Opération annulée !");
                return;
            }
            //Si le test passe c'est que le choix de l'utilisateur est valide
            if (validerChoixExtension(choixExtension)) {
                if (processChoixExensions(Integer.parseInt(choixExtension)))
                {
                    choixValide = true;
                } 
                else {
                    System.out.println("Choix de d'extension invalide ! La longueur totale dépasse 60cm.");
                }
            } else {
                System.out.println("Choix de d'extension invalide ! Veuillez réessayer.");
            }
        }
    }

   public boolean processChoixExensions(int choixExtension) {
        int longueur = choixExtension;
        int longueureMaximaleCheveux = 60;
        if ((longueur + this.client.getLongeurCheveux()) <= longueureMaximaleCheveux) {
            System.out.println("Retouches de la coupe en cours...");
            //Méthodes try catch permettant d'utiliser la méthode thread.sleep
            try {
                int tempsCoupe = 3000;
                Thread.sleep(tempsCoupe);
                this.client.setLongeurCheveux(longueur + this.client.getLongeurCheveux());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Retouches terminées !");
            return true;
        }
        return false;
    }
 
    
    /**
     * Valide le choix d'extension saisi par l'utilisateur.
     *
     * @param choix La chaîne représentant le choix d'extension.
     * @return true si le choix est valide, false sinon.
     *
     */
    private boolean validerChoixExtension(String choix) {
        //On vérifie que le choix de l'utilisateur est bien dans la plage de valeur attendue
        try {
            int choixInt = Integer.parseInt(choix);
            return choixInt >= 1 && choixInt <= 40;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void facturerClient() {
        System.out.println("Facturation pour le coiffeur");
    }
}
