package src.com.myproject;
/**
 * La classe Voiture représente un type particulier de véhicule, héritant de la classe Vehicule.
 * Elle définit les caractéristiques spécifiques d'une voiture, telles que la marque, le modèle et la couleur.
 */
public class Voiture extends Vehicule {

    /**
     * Constructeur de la classe Voiture.
     *
     * @param marque  La marque de la voiture.
     * @param modele  Le modèle de la voiture.
     * @param couleur La couleur de la voiture.
     */
    public Voiture(String marque, String modele, String couleur) {
        // Appel du constructeur de la classe parente (Vehicule) avec les paramètres spécifiés.
        super(marque, modele, couleur);
    }
}
