package src.com.myproject;
import java.util.List;

/**
 * La classe Personnage représente un personnage avec des caractéristiques telles que le pseudo, le sexe, la taille,
 * la longueur des cheveux et une liste de vêtements.
 */
public class Personnage {

    private String Pseudo;               // Pseudo du personnage
    private String Sexe;                 // Sexe du personnage
    private int Taille;                   // Taille du personnage
    private int LongeurCheveux;          // Longueur des cheveux du personnage
    private List<Vetement> lstVetement;  // Liste des vêtements du personnage

    /**
     * Constructeur de la classe Personnage.
     *
     * @param pseudo       Le pseudo du personnage.
     * @param sexe         Le sexe du personnage.
     * @param taille       La taille du personnage.
     * @param longeurCheveux La longueur des cheveux du personnage.
     * @param lstVetement  La liste des vêtements du personnage.
     */
    public Personnage(String pseudo, String sexe, int taille, int longeurCheveux, List<Vetement> lstVetement) {
        Pseudo = pseudo;
        Sexe = sexe;
        Taille = taille;
        LongeurCheveux = longeurCheveux;
        this.lstVetement = lstVetement;
    }

    /**
     * Getter pour récupérer le pseudo du personnage.
     *
     * @return Le pseudo du personnage.
     */
    public String getPseudo() {
        return Pseudo;
    }

    /**
     * Setter pour définir le pseudo du personnage.
     *
     * @param pseudo Le nouveau pseudo du personnage.
     */
    public void setPseudo(String pseudo) {
        Pseudo = pseudo;
    }

    /**
     * Getter pour récupérer le sexe du personnage.
     *
     * @return Le sexe du personnage.
     */
    public String getSexe() {
        return Sexe;
    }

    /**
     * Setter pour définir le sexe du personnage.
     *
     * @param sexe Le nouveau sexe du personnage.
     */
    public void setSexe(String sexe) {
        Sexe = sexe;
    }

    /**
     * Getter pour récupérer la taille du personnage.
     *
     * @return La taille du personnage.
     */
    public int getTaille() {
        return Taille;
    }

    /**
     * Setter pour définir la taille du personnage.
     *
     * @param taille La nouvelle taille du personnage.
     */
    public void setTaille(int taille) {
        Taille = taille;
    }

    /**
     * Getter pour récupérer la longueur des cheveux du personnage.
     *
     * @return La longueur des cheveux du personnage.
     */
    public int getLongeurCheveux() {
        return LongeurCheveux;
    }

    /**
     * Setter pour définir la longueur des cheveux du personnage.
     *
     * @param longeurCheveux La nouvelle longueur des cheveux du personnage.
     */
    public void setLongeurCheveux(int longeurCheveux) {
        LongeurCheveux = longeurCheveux;
    }

    /**
     * Méthode pour calculer la taille totale du personnage, en prenant en compte la taille des chaussures s'il en porte.
     *
     * @return La taille totale du personnage.
     */
    public int getTailleTotale() {
        int taille = this.Taille;
        for (Vetement unVetement : this.lstVetement) {
            if (unVetement.getType().equals("Chaussures")) {
                int tailleChaussure = convertisseurTaille(unVetement.getTaille());
                if (tailleChaussure != -1) {
                    return taille + tailleChaussure;
                }
            }
        }
        return this.Taille;
    }

    /**
     * Méthode de conversion de la taille des chaussures en centimètres.
     *
     * @param tailleRecherchee La taille des chaussures à convertir.
     * @return La taille des chaussures en centimètres, -1 si la conversion n'est pas possible.
     */
    public int convertisseurTaille(Tailles tailleRecherchee) {
        int index = -1;

        for (Tailles taille : Tailles.values()) {
            index++;
            if (taille == tailleRecherchee) {
                return index * 5;
            }
        }
        return -1;
    }

    /**
     * Méthode toString pour obtenir une représentation textuelle du personnage.
     *
     * @return Une chaîne de caractères représentant le personnage.
     */
    @Override


    public String toString() {
        return "Personnage{" +
                "Pseudo='" + Pseudo + '\'' +
                ", Sexe='" + Sexe + '\'' +
                ", Taille=" + Taille +
                ", LongeurCheveux=" + LongeurCheveux +
                ", lstVetement=" + lstVetement +
                '}';
    }
}
