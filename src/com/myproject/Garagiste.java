package src.com.myproject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Classe représentant un Garagiste dans le jeu, implémentant l'interface Metier.
 */
public class Garagiste implements Metier {

    private Map<Integer, String> competences;
    private Vehicule vehiculeDansGarage;

    /**
     * Constructeur de la classe Garagiste.
     * Initialise les compétences du garagiste et le véhicule dans le garage à null.
     */
    public Garagiste() {
        this.competences = new HashMap<>();
        this.vehiculeDansGarage = null;
        this.competences.put(1, "Réparer la carrosserie");
        this.competences.put(2, "Réparer le moteur");
        this.competences.put(3, "Refaire la peinture");
    }

    /**
     * Permet à un véhicule d'entrer dans le garage.
     *
     * @param unVehicule Le véhicule qui entre dans le garage.
     * @return true si l'entrée est réussie, false si le garage est occupé.
     */
    public boolean entrerGarage(Vehicule unVehicule) {
        if (garageEstVide()) {
            System.out.println("Entrée dans le garage....");
            this.vehiculeDansGarage = unVehicule;
            return true;
        } else {
            System.out.println("Un véhicule est déjà dans le garage !");
            return false;
        }
    }

    /**
     * Permet à un véhicule de sortir du garage.
     *
     * @return true si la sortie est réussie, false si le garage est vide.
     */
    public boolean sortieGarage() {
        if (!garageEstVide()) {
            System.out.println("Sortie du garage....");
            this.vehiculeDansGarage = null;
            return true;
        } else {
            System.out.println("Aucun véhicule n'est dans le garage !");
            return false;
        }
    }

    /**
     * Vérifie si le garage est vide.
     *
     * @return true si le garage est vide, false sinon.
     */
    public boolean garageEstVide() {
        return this.vehiculeDansGarage == null;
    }

    @Override
    public List<String> listerCompetences() {
        System.out.println("Liste des compétences :");
        System.out.println();
        competences.forEach((id, competence) -> System.out.println(id + ". " + competence));
        return List.copyOf(competences.values());
    }

    @Override
    public void effectuerLeTravail(int idCompetence) {
        if (!garageEstVide()) {
            if (competences.containsKey(idCompetence)) {
                System.out.println("Compétence sélectionnée : " + competences.get(idCompetence));

                switch (idCompetence) {
                    case 1:
                        reparerCarrosserie();
                        break;
                    case 2:
                        reparerMoteur();
                        break;
                    case 3:
                        refairePeinture();
                        break;
                    default:
                        System.out.println("Compétence inconnue");
                }
            } else {
                System.out.println("Compétence inconnue");
            }
        } else {
            System.out.println("Il n'y a aucun véhicule dans le garage !");
        }
    }

    // Méthodes privées pour effectuer le travail spécifique du Garagiste

    /**
     * Répare la carrosserie du véhicule dans le garage.
     */
    private void reparerCarrosserie() {
        System.out.println("Réparation de la carrosserie en cours...");
        try {
            int tempsCoupe = 3000;
            int maxPtsDeVie = 100;
            Thread.sleep(tempsCoupe);
            this.vehiculeDansGarage.setpointsDeVie(maxPtsDeVie);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Réparations terminées !");
    }

    /**
     * Répare le moteur du véhicule dans le garage.
     */
    private void reparerMoteur() {
        System.out.println("Réparation du moteur en cours...");
        try {
            int tempsCoupe = 3000;
            int maxPtsDeVie = 100;
            Thread.sleep(tempsCoupe);
            this.vehiculeDansGarage.setMoteur(maxPtsDeVie);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Réparations terminées !");
    }

    /**
     * Refait la peinture du véhicule dans le garage.
     */
    private void refairePeinture() {
        boolean choixValide = false;

        Map<Integer, String> couleursMap = new HashMap<>();
        couleursMap.put(1, "Rouge");
        couleursMap.put(2, "Bleu");
        couleursMap.put(3, "Vert");
        couleursMap.put(4, "Jaune");
        couleursMap.put(5, "Noir");
        couleursMap.put(6, "Blanc");
        couleursMap.put(7, "Gris");
        couleursMap.put(8, "Orange");


        //Boucle tant que l'utilisateur ne saisie pas un choix valide.
        while (!choixValide) {
            System.out.println("Choisissez une couleur parmi les options suivantes:");
            couleursMap.forEach((num, couleur) -> System.out.println(num + ". " + couleur));
            System.out.println("Entrez 'exit' pour annuler l'opération.");

            Scanner scanner = new Scanner(System.in);
            String choixCouleur = scanner.nextLine();

            if (choixCouleur.equalsIgnoreCase("exit")) {
                System.out.println("Opération annulée !");
                return;
            }

            //Si le texst passe c'est que le choix est valide
            if (validerChoixCouleur(choixCouleur)) {
                System.out.println("Retouches de la peinture en cours...");
                //Méthodes try catch afin de pouvoir utiliser la méthode thread sleep
                try {
                    int tempsCoupe = 3000;
                    Thread.sleep(tempsCoupe);
                    int choixInt = Integer.parseInt(choixCouleur);
                    this.vehiculeDansGarage.setCouleur(couleursMap.get(choixInt));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Retouches terminées !");
                choixValide = true;
            } else {
                System.out.println("Choix de couleur invalide ! Veuillez réessayer.");
            }
        }
    }

    /**
     * Valide le choix de couleur saisi par l'utilisateur.
     *
     * @param choix La chaîne représentant le choix de couleur.
     * @return true si le choix est valide, false sinon.
     */
    private boolean validerChoixCouleur(String choix) {
        //On vérifie que le choix de l'utilisateur est bien dans la plage de valeur attendue
        try {
            int choixInt = Integer.parseInt(choix);
            return choixInt >= 1 && choixInt <= 8;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void facturerClient() {
        System.out.println("Facturation pour le garagiste");
    }
}
