package test.com.myproject.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import src.com.myproject.Coiffeur;
import src.com.myproject.Personnage;
import src.com.myproject.Tailles;
import src.com.myproject.Vetement;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

public class CoiffeurTest {

    private Coiffeur coiffeur;
    private Personnage personnage;

    @BeforeEach
    public void setUp() {
        this.coiffeur = new Coiffeur();

        List<Vetement> lstVetement = new ArrayList<Vetement>();
        Vetement unVetement;

        // Create a simple instance of Personnage for testing
        lstVetement.add(unVetement = new Vetement("T-shirt", "Blanc", Tailles.M));
        lstVetement.add(unVetement = new Vetement("Pantalon", "Bleu", Tailles.L));
        lstVetement.add(unVetement = new Vetement("Chaussures", "Rouge", Tailles.XXXL));

        this.personnage = new Personnage("Test Character", "homme", 180, 8, lstVetement);
        this.personnage.setLongeurCheveux(10); // Initial hair length for testin

        this.coiffeur.sortieCoiffeur();
    }

    @Test
    public void testEntrerCoiffeurWhenEmpty() {
        boolean result = this.coiffeur.entrerCoiffeur(this.personnage);
        assertTrue(result, "Should allow entry when the salon is empty");
        assertFalse(this.coiffeur.coiffeurEstVide(), "The salon should not be empty after a client enters");
    }

    @Test
    public void testEntrerCoiffeurWhenOccupied() {
        this.coiffeur.entrerCoiffeur(this.personnage);
        this.personnage.setLongeurCheveux(15);

        boolean result = this.coiffeur.entrerCoiffeur(this.personnage);
        assertFalse(result, "Should not allow entry when the salon is already occupied");
    }

    @Test
    public void testSortieCoiffeurWhenOccupied() {
        this.coiffeur.entrerCoiffeur(this.personnage);
        boolean result = this.coiffeur.sortieCoiffeur();
        assertTrue(result, "Should allow exit when there is a client");
        assertTrue(this.coiffeur.coiffeurEstVide(), "The salon should be empty after the client exits");
    }

    @Test
    public void testSortieCoiffeurWhenEmpty() {
        boolean result = this.coiffeur.sortieCoiffeur();
        assertFalse(result, "Should not allow exit when the salon is empty");
    }

    @Test
    public void testCoiffeurEstVide() {
        assertTrue(this.coiffeur.coiffeurEstVide(), "The salon should be empty initially");
        this.coiffeur.entrerCoiffeur(this.personnage);
        assertFalse(this.coiffeur.coiffeurEstVide(), "The salon should not be empty after a client enters");
    }

    @Test
    public void testListerCompetences() {
        var competences = this.coiffeur.listerCompetences();
        assertEquals(2, competences.size(), "There should be 2 competences listed");
        assertTrue(competences.contains("Faire une coupe"));
        assertTrue(competences.contains("Poser des extensions"));
    }


    @Test
    public void testFaireUneCoupeValidChoice() {
        this.coiffeur.entrerCoiffeur(this.personnage);

        // Simulate the process of choosing a haircut directly
        this.coiffeur.processChoixCoupe(1 );

        // Assuming the valid haircut reduces hair length to 0 for simplicity
        assertEquals(0, this.personnage.getLongeurCheveux(), "Hair length should be updated after the valid haircut choice");
    }

    @Test
    public void testPoserExtensionsValidChoice() {
        this.coiffeur.entrerCoiffeur(this.personnage);
    
        // Simulate the process of choosing extensions
        this.coiffeur.processChoixExensions(5);
    
        // Assuming the valid extension adds length to the hair
        assertTrue(this.personnage.getLongeurCheveux() > 10, "Hair length should increase after applying extensions");
    }

    @Test
    public void testFacturerClient() {
        // Basic test for the facturerClient method
        this.coiffeur.facturerClient();

        // You could verify output or any changes in state if applicable
    }
}
